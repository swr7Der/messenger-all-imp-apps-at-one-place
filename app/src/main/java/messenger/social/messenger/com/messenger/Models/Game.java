package messenger.social.messenger.com.messenger.Models;


import com.google.gson.annotations.SerializedName;

public class Game {


    @SerializedName("games")
    private SingleGame[] games;

    public SingleGame[] getGames() {
        return this.games;
    }

    public void setGames(SingleGame[] games) {
        this.games = games;
    }
}
