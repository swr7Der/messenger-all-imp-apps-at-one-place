package messenger.social.messenger.com.messenger.Models;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import messenger.social.messenger.com.messenger.Helper.packageHelper;
import messenger.social.messenger.com.messenger.R;

import static messenger.social.messenger.com.messenger.Helper.MobVistaHelper.loadNative;

/**
 * Created by mohak on 19/1/17.
 */

public class MessengerItemHolder extends RecyclerView.ViewHolder {

    public TextView appName, usage, ad;
    public ImageView appIcon;
    public LinearLayout parent;
    public  int i;
    public MessengerItemHolder(View view, int i) {
        super(view);
        this.i=i;
        appName = (TextView) view.findViewById(R.id.app_name);
        appIcon = (ImageView) view.findViewById(R.id.app_icon);
        parent = (LinearLayout) view.findViewById(R.id.parentLinear);
        usage = (TextView) view.findViewById(R.id.percentageUse);
        ad = (TextView) itemView.findViewById(R.id.ad);
    }


    public void bindData(Object currPackage) {


        if (i==1){
            appName.setText(((SingleGame) currPackage).getGameName());
            Glide.with(itemView.getContext()).load(((SingleGame) currPackage).getGameImg()).into(appIcon);
            return;
        }
        usage.setVisibility(View.VISIBLE);
        ad.setVisibility(View.GONE);

        if (currPackage instanceof SinglePackage) {

            ((SinglePackage) currPackage).setImgId(packageHelper.getAppDrawable(((SinglePackage) currPackage).getPackageName(), itemView.getContext()));
            appName.setText(packageHelper.getAppName(((SinglePackage) currPackage).getPackageName(), itemView.getContext()));
            appIcon.setImageDrawable(((SinglePackage) currPackage).getImgId());
            if (SinglePackage.getTotal_click_counter() != 0)
                usage.setText("" + ((SinglePackage) currPackage).getClick_counter() + "X (" + ((SinglePackage) currPackage).getClick_counter() * 100 / SinglePackage.getTotal_click_counter() + "%)");
            else
                usage.setText("" + ((SinglePackage) currPackage).getClick_counter() + "X (0%)");

        } else if (currPackage instanceof String) {

            usage.setVisibility(View.INVISIBLE);
            appIcon.setImageResource(R.drawable.explore_apps_min);
            appName.setText((String) currPackage);

        } else if (currPackage instanceof SingleGame) {

            usage.setVisibility(View.INVISIBLE);
            appName.setText(((SingleGame) currPackage).getGameName());
            Glide.with(itemView.getContext()).load(((SingleGame) currPackage).getGameImg()).into(appIcon);

        } else {

            //display ad
            ad.setVisibility(View.VISIBLE);
            usage.setVisibility(View.GONE);
            loadNative(itemView.getContext(), appIcon, appName, null, null,ad);
        }
    }


}
