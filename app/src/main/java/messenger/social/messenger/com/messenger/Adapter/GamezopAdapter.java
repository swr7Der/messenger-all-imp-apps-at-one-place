package messenger.social.messenger.com.messenger.Adapter;

/**
 * Created by mohak on 9/2/17.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import messenger.social.messenger.com.messenger.Activities.WebViewZop;
import messenger.social.messenger.com.messenger.Helper.Constants;
import messenger.social.messenger.com.messenger.Helper.Database;
import messenger.social.messenger.com.messenger.Models.MessengerItemHolder;
import messenger.social.messenger.com.messenger.Models.SingleGame;
import messenger.social.messenger.com.messenger.R;


public class GamezopAdapter extends RecyclerView.Adapter<MessengerItemHolder> {

    List<SingleGame> gameList;
    Context context;

    public GamezopAdapter(List<SingleGame> gameList , Context context) {
        this.gameList = gameList;
        this.context = context;
    }

    @Override
    public MessengerItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_messenger, parent, false);
        return new MessengerItemHolder(view,1);
    }

    @Override
    public void onBindViewHolder(final MessengerItemHolder holder, final int position) {

        holder.bindData(gameList.get(position));
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(context , WebViewZop.class);
                intent.putExtra(Constants.gameLink , gameList.get(holder.getAdapterPosition()).getGameUrl());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return gameList == null ? 0 : gameList.size();
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}