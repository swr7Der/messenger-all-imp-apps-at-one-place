package messenger.social.messenger.com.messenger.Helper;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.mobvista.msdk.MobVistaSDK;
import com.mobvista.msdk.out.MobVistaSDKFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by mohak on 23/1/17.
 */

public class Messenger extends Application {

    static SharedPreferences navdrawerPrefrence, firsttimePrefrence;
    private MobVistaSDK sdk;
    private static Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();
        navdrawerPrefrence = getSharedPreferences(Constants.fileName, MODE_PRIVATE);
        firsttimePrefrence = getSharedPreferences(Constants.fileName2, MODE_PRIVATE);
        sdk = MobVistaSDKFactory.getMobVistaSDK();
        Map<String, String> map = sdk.getMVConfigurationMap("32693", "efb421f09a1cc1aab6f5bcf9c11c0546");
        sdk.init(map, this);
    }

    public static SharedPreferences getNavSharedPref() {
        return navdrawerPrefrence;
    }

    public static SharedPreferences getFirsttimePrefrence() {
        return firsttimePrefrence;
    }

    public static Retrofit getRetroInstance() {
        if (retrofit == null) {
            return new Retrofit.Builder()
                    .baseUrl("https://pub.gamezop.com/").addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
