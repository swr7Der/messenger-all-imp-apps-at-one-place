package messenger.social.messenger.com.messenger.Models;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import messenger.social.messenger.com.messenger.Activities.GamezopActivity;
import messenger.social.messenger.com.messenger.Activities.WebViewZop;
import messenger.social.messenger.com.messenger.Adapter.RecyclerAdapter;
import messenger.social.messenger.com.messenger.Helper.Constants;
import messenger.social.messenger.com.messenger.Helper.GameFetcherService;
import messenger.social.messenger.com.messenger.Helper.IOHelper;
import messenger.social.messenger.com.messenger.Helper.Messenger;
import messenger.social.messenger.com.messenger.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mohak on 9/2/17.
 */
public class RecyclerHolder extends RecyclerView.ViewHolder implements RecyclerAdapter.clickCallback {

    RecyclerView recyclerView;
    List<SingleGame> gameZopData = new ArrayList<>();
    private RecyclerAdapter adapter;

    public RecyclerHolder(View itemView) {
        super(itemView);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.gameRec);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(itemView.getContext(), 1, GridLayoutManager.HORIZONTAL, false);


        recyclerView.setLayoutManager(gridLayoutManager);

        if (new File(itemView.getContext().getFilesDir() + "/cache").exists()){

            gameZopData = IOHelper.readfromFile("cache", itemView.getContext()).subList(0, 7);
            adapter = new RecyclerAdapter(this.itemView.getContext(), gameZopData);
            recyclerView.setAdapter(adapter);
            adapter.setCallback(RecyclerHolder.this);

        }else if (IOHelper.isNetworkAvailable(this.itemView.getContext().getApplicationContext())){

            fetchDatafromGameZop();
        }
    }


    /**
     * Fetches data from gamezop Api
     */
    void fetchDatafromGameZop() {

        GameFetcherService gameFetcherService = Messenger.getRetroInstance().create(GameFetcherService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id", Constants.API_KEY);
        param.put("appendParams", "true");
        gameFetcherService.getTasks(param).enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {

                if (response.body() != null) {

                    IOHelper.writeToFile(Arrays.asList(response.body().getGames()), itemView.getContext(), "cache");
                    gameZopData.addAll(Arrays.asList(response.body().getGames()).subList(0, 7));
                    adapter = (new RecyclerAdapter(itemView.getContext(), gameZopData));
                    recyclerView.setAdapter(adapter);
                    adapter.setCallback(RecyclerHolder.this);
                }
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {


            }
        });

    }

    @Override
    public void click(Object currPackage) {

        Intent intent = new Intent(itemView.getContext(), WebViewZop.class);
        intent.putExtra(Constants.gameLink, ((SingleGame) currPackage).getGameUrl());
        itemView.getContext().startActivity(intent);
    }
}
