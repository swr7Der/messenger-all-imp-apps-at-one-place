package messenger.social.messenger.com.messenger.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import messenger.social.messenger.com.messenger.Models.SinglePackage;

/**
 * Created by mohak on 2/2/17.
 */

public class Database extends SQLiteOpenHelper {

    private static Database sInstance;
    public static final String UID = "_id";
    public static final String DATABASE_NAME = "messengerdb";
    public static final int DATABASE_VERSION = 1;
    public static final String PACKAGENAME = "PackageName";
    public static final String TABLENAME = "appsdata";
    public static final String COUNTER = "counter";
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLENAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PACKAGENAME + " VARCHAR(255), " + COUNTER + " INTEGER);";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public static Database getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null) {
            sInstance = new Database(context);
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void findAppInDb(Database instance, String appPackage) {

        SQLiteDatabase sqldb = instance.getReadableDatabase();
        String app;
        int counter;
        Cursor cursor = sqldb.query(Database.TABLENAME, new String[]{Database.PACKAGENAME, Database.COUNTER}, Database.PACKAGENAME + "=?", new String[]{appPackage}, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToNext()) {

            app = cursor.getString(cursor.getColumnIndex(Database.PACKAGENAME));
            counter = cursor.getInt(cursor.getColumnIndex(Database.COUNTER));
            updateCounter(instance, app, counter);

        } else {

            Log.d(Constants.TAG, "Error");
        }

        if (cursor != null) {
            cursor.close();
        }
    }


    public void updateCounter(Database instance, String appPackage, int counter) {

        SQLiteDatabase sqldb = instance.getWritableDatabase();
        ContentValues values = new ContentValues();
        counter = counter + 1;
        values.put(Database.COUNTER, counter);
        sqldb.update(Database.TABLENAME, values, Database.PACKAGENAME + "=?", new String[]{appPackage});
    }


    public int totalClick(Database instance) {

        int sum = 0;
        SQLiteDatabase sqldb = instance.getReadableDatabase();
        Cursor cursor = sqldb.query(Database.TABLENAME, new String[]{Database.COUNTER}, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                sum = sum + cursor.getInt(cursor.getColumnIndex(Database.COUNTER));
            }
        }

        if (cursor != null) {
            cursor.close();
        }

        return sum;
    }


    public ArrayList<Object> readAllApps(Database instance) {

        int sum = 0;
        SQLiteDatabase sqldb = instance.getReadableDatabase();
        ArrayList<Object> data = new ArrayList<>();
        Cursor cursor = sqldb.query(Database.TABLENAME, new String[]{Database.PACKAGENAME, Database.COUNTER}, null, null, null, null, Database.COUNTER + " DESC");
        if (cursor != null && cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                SinglePackage singlePackage = new SinglePackage();
                singlePackage.setPackageName(cursor.getString(cursor.getColumnIndex(Database.PACKAGENAME)));
                singlePackage.setClick_counter(cursor.getInt(cursor.getColumnIndex(Database.COUNTER)));
                sum = sum + singlePackage.getClick_counter();
                data.add(singlePackage);
            }
        }

        SinglePackage.setTotal_click_counter(sum);
        if (cursor != null) {
            cursor.close();
        }

        return data;
    }

    /**
     * add packageNames installed to database
     *
     * @param instance    Instance of {@link Database}
     * @param packageName list of installed packages
     */
    public void addTodb(Database instance, ArrayList<Object> packageName) {

        SQLiteDatabase sqldb = instance.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < packageName.size(); i++) {
            if (packageName.get(i) instanceof SinglePackage) {
                SinglePackage singlePackage = (SinglePackage) packageName.get(i);
                values.put(Database.PACKAGENAME, singlePackage.getPackageName());
                values.put(Database.COUNTER, singlePackage.getClick_counter());
                sqldb.insert(Database.TABLENAME, null, values);
            }
        }
    }


}
