package messenger.social.messenger.com.messenger.Helper;

import java.util.Map;

import messenger.social.messenger.com.messenger.Models.Game;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by mohak on 9/2/17.
 */

public interface GameFetcherService {

    @GET("v2/games")
    Call<Game> getTasks(@QueryMap Map<String,String> options);
}