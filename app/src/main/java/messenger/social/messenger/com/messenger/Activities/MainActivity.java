package messenger.social.messenger.com.messenger.Activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


import messenger.social.messenger.com.messenger.Adapter.MessengerListAdapter;
import messenger.social.messenger.com.messenger.Helper.Constants;
import messenger.social.messenger.com.messenger.Helper.Database;
import messenger.social.messenger.com.messenger.Helper.Messenger;
import messenger.social.messenger.com.messenger.Helper.MobVistaHelper;
import messenger.social.messenger.com.messenger.Helper.NotificationHelper;
import messenger.social.messenger.com.messenger.Helper.IOHelper;
import messenger.social.messenger.com.messenger.Helper.packageHelper;
import messenger.social.messenger.com.messenger.Models.SingleAd;
import messenger.social.messenger.com.messenger.Models.SingleGame;
import messenger.social.messenger.com.messenger.Models.SinglePackage;
import messenger.social.messenger.com.messenger.R;
import messenger.social.messenger.com.messenger.Service.FloatingBubble;

import static messenger.social.messenger.com.messenger.Helper.MobVistaHelper.preloadNative;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MessengerListAdapter.clickCallback, View.OnClickListener {

    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    private int mNotificationId = 1;
    private ArrayList<Object> packageName;
    private RecyclerView packageGrid;
    private SharedPreferences navDrawerPref, firstTimePref;
    private MessengerListAdapter adapter;
    private NavigationView navigationView;
    private ProgressBar progressBar;
    private boolean paused;
    private TextView totalUsed;
    private ImageView share, freeApps, gamezop;
    private Database database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        packageGrid = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        totalUsed = (TextView) findViewById(R.id.total_used);
        share = (ImageView) findViewById(R.id.share);
        freeApps = (ImageView) findViewById(R.id.free_toolbar);
        gamezop = (ImageView) findViewById(R.id.gamezop);

        share.setOnClickListener(this);
        freeApps.setOnClickListener(this);
        gamezop.setOnClickListener(this);

        firstTimePref = Messenger.getFirsttimePrefrence();
        packageName = new ArrayList<>();

        navDrawerPref = Messenger.getNavSharedPref();

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().findItem(R.id.enableNotification).
                getActionView().findViewById(R.id.drawer_switch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (((Switch) view).isChecked()) {

                    IOHelper.sharedPrefEdit(navDrawerPref, true, Constants.NotificationSwitch);
                    NotificationHelper.setUpNotification(MainActivity.this, getString(R.string.title), getString(R.string.contentText), mNotificationId);

                } else {

                    IOHelper.sharedPrefEdit(navDrawerPref, false, Constants.NotificationSwitch);
                    NotificationHelper.cancelNotification(MainActivity.this, mNotificationId);
                }
            }
        });

        database = Database.getInstance(getApplicationContext());
        askForPermission();
        initialise();
    }

    private void setUpDb() {

        /*Read from constant and insert data to database only once after that use db to get sorted order based onn usage*/
        if (firstTimePref.getBoolean(Constants.first, true)) {

            for (int i = 0; i < Constants.packageName.length; i++) {

                if (packageHelper.isPackageInstalled(Constants.packageName[i], getPackageManager())) {
                    SinglePackage singlePackage = new SinglePackage();
                    singlePackage.setPackageName(Constants.packageName[i]);
                    singlePackage.setClick_counter(0);
                    packageName.add(singlePackage);
                }
            }

            if (Telephony.Sms.getDefaultSmsPackage(this) != null) {

                SinglePackage pack = new SinglePackage();
                pack.setClick_counter(0);
                pack.setPackageName(Telephony.Sms.getDefaultSmsPackage(this));
                packageName.add(pack);
            }

            database.addTodb(Database.getInstance(this.getApplicationContext()), packageName);
            if (!packageName.isEmpty())
                firstTimePref.edit().putBoolean(Constants.first, false).apply();
        } else {
            packageName = database.readAllApps(Database.getInstance(this.getApplicationContext()));
        }

        packageName.add("Free Apps");
        if (IOHelper.isNetworkAvailable(getApplicationContext())) {

            packageName.add(null);
            packageName.add(0.1);
            packageName.add(1);
            packageName.add(new SingleAd());

        } else {

            packageName.add(0.1);
            packageName.add(1);
        }

        setUpManager();

        totalUsed.setText("Total Used: " + database.totalClick(Database.getInstance(getApplicationContext())) + "X");


    }

    public void askForPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                setUpDb();

            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            setUpDb();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //resume tasks needing this permission
            setUpDb();
        } else {

            Toast.makeText(this, "Storage Permission required for proper functioning", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * initialise switch status in nav drawer
     */
    private void initialise() {

        if (navDrawerPref.getBoolean(Constants.NotificationSwitch, true)) {
            NotificationHelper.setUpNotification(MainActivity.this, getString(R.string.title), getString(R.string.contentText), mNotificationId);
            ((Switch) navigationView.getMenu().findItem(R.id.enableNotification).getActionView().findViewById(R.id.drawer_switch)).setChecked(true);
        } else
            ((Switch) navigationView.getMenu().findItem(R.id.enableNotification).getActionView().findViewById(R.id.drawer_switch)).setChecked(false);

    }


    /**
     * set up grid layout manager
     */
    private void setUpManager() {

        adapter = new MessengerListAdapter(this, packageName);
        adapter.setCallback(this);
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (packageName.get(position) instanceof String || packageName.get(position) instanceof SinglePackage || packageName.get(position) == null
                        || packageName.get(position) instanceof SingleGame)
                    return 1;
                else
                    return 3;
            }
        });
        packageGrid.setLayoutManager(manager);
        packageGrid.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobVistaHelper.preloadWall();
        MobVistaHelper.loadHandler(this);
        preloadNative();

        if (database.totalClick(Database.getInstance(getApplicationContext())) > 0)
            totalUsed.setVisibility(View.VISIBLE);
        if (paused) {

            resetAdap();
        }

        if (IOHelper.isNetworkAvailable(this.getApplicationContext()) && !new File(getFilesDir() + "/cache").exists()) {

            resetAdap();
        }

    }

    private void resetAdap() {


        packageName = Database.getInstance(getApplicationContext()).readAllApps(Database.getInstance(this.getApplicationContext()));
        packageName.add("Free Apps");

        if (IOHelper.isNetworkAvailable(getApplicationContext())) {

            packageName.add(null);
            packageName.add(0.1);
            packageName.add(1);
            packageName.add(new SingleAd());

        } else {

            packageName.add(0.1);
            packageName.add(1);
        }

        setUpManager();
        totalUsed.setText("Total Used: " + database.totalClick(Database.getInstance(getApplicationContext())) + "X");

    }

    @Override
    protected void onPause() {
        paused = true;
        super.onPause();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.enableNotification) {


        } else if (id == R.id.rate) {

            rate();

        } else if (id == R.id.share) {

            IOHelper.shareApp(this);

        } else if (id == R.id.feedback) {

            feedback(-1);
        } else if (id == R.id.download_all_in_one) {

            openPlayStore2();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Open play store to All in One Socail Media App
     */
    private void openPlayStore2() {

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.socialmedianetwork.mail.browser")));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.socialmedianetwork.mail.browser")));
        }

    }

    /**
     * feedback of app via gmail or any other email client if gmail is not installed
     *
     * @param rating -1 for normal feedback else feedback from rating bar
     */
    private void feedback(int rating) {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"nudimelabs@gmail.com"});

        Intent i = new Intent(Intent.ACTION_SEND);
        i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"nudimelabs@gmail.com"});
        i.setType("plain/text");

        if (rating == -1) {
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback for Messenger app");
            i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback for Messenger app");
        } else {
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback for Messenger app. Rated " + rating + "/5");
            i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback for Messenger app");
        }

        try {

            startActivity(emailIntent);

        } catch (Exception e) {

            startActivity(Intent.createChooser(i, "Feedback for Messenger"));

        }

    }

    /**
     * Allows user to rate app based on their experience
     */
    private void rate() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        builder.setCancelable(true);
        builder.setTitle("Love this app ?").setMessage("Please take a moment to rate us");
        builder.setView(inflater.inflate(R.layout.rating, null)).setPositiveButton("RATE NOW", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                RatingBar bar = (RatingBar) ((AlertDialog) dialogInterface).findViewById(R.id.ratingVal);

                if (bar != null) {
                    if (bar.getRating() != 5)
                        feedback((int) bar.getRating());
                    else
                        openPlayStore();

                }
            }
        }).setNegativeButton("LATER", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.GRAY);

    }


    /**
     * open app directly in play store
     */
    private void openPlayStore() {

        final String appPackageName = "messenger.social.messenger.com.messenger";
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    /**
     * show bubble if permission given else ask for it
     */
    private void showBubble() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);


        } else {

            IOHelper.sharedPrefEdit(navDrawerPref, true, Constants.BubbleSwitch);
            startService(new Intent(this, FloatingBubble.class));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
            //Check if the permission is granted or not.
            if (resultCode == RESULT_OK) {
                IOHelper.sharedPrefEdit(navDrawerPref, true, Constants.BubbleSwitch);
                startService(new Intent(this, FloatingBubble.class));

            } else {
                //Permission is not available
//                ((Switch) navigationView.getMenu().findItem(R.id.enableBubble).getActionView().findViewById(R.id.drawer_switch)).setChecked(false);
            }
        }
    }


    @Override
    public void click(Object currPackage) {

        if (currPackage instanceof SinglePackage) {


            findAppInDb(Database.getInstance(getApplicationContext()), ((SinglePackage) currPackage).getPackageName());

            try {

                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(((SinglePackage) currPackage).getPackageName());
                startActivity(launchIntent);

            } catch (Exception e) {

                Toast.makeText(this, "Something went wrong please try again !", Toast.LENGTH_SHORT).show();
            }

        } else if (currPackage instanceof String) {

            MobVistaHelper.openAdWall(this);

        } else if (currPackage instanceof SingleGame) {

            Intent intent = new Intent(this, WebViewZop.class);
            intent.putExtra(Constants.gameLink, ((SingleGame) currPackage).getGameUrl());
            startActivity(intent);
        }

    }


    /**
     * @param instance   Instance of {@link Database}
     * @param appPackage packName of app whose counter needs to be updated
     * @param counter    previous value o counter
     */
    public void updateCounter(Database instance, String appPackage, int counter) {

        SQLiteDatabase sqldb = instance.getWritableDatabase();
        ContentValues values = new ContentValues();
        counter = counter + 1;
        values.put(Database.COUNTER, counter);
        sqldb.update(Database.TABLENAME, values, Database.PACKAGENAME + "=?", new String[]{appPackage});

        for (int i = 0; i < packageName.size(); i++)
            if (packageName.get(i) instanceof SinglePackage && ((SinglePackage) packageName.get(i)).getPackageName().equals(appPackage)) {

                ((SinglePackage) packageName.get(i)).setClick_counter(counter);

            }
    }

    /**
     * query to find the required packageName in database
     *
     * @param instance   Instance of {@link Database}
     * @param appPackage packageName of app whose counter needs to be updated
     */
    public void findAppInDb(Database instance, String appPackage) {

        SQLiteDatabase sqldb = instance.getReadableDatabase();
        String app;
        int counter;
        Cursor cursor = sqldb.query(Database.TABLENAME, new String[]{Database.PACKAGENAME, Database.COUNTER}, Database.PACKAGENAME + "=?", new String[]{appPackage}, null, null, null);
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToNext()) {

            app = cursor.getString(cursor.getColumnIndex(Database.PACKAGENAME));
            counter = cursor.getInt(cursor.getColumnIndex(Database.COUNTER));
            updateCounter(instance, app, counter);

        } else {

            Log.d(Constants.TAG, "Error");
        }

        if (cursor != null) {
            cursor.close();
        }
    }


    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.share)
            IOHelper.shareApp(this);
        else if (view.getId() == R.id.free_toolbar)
            MobVistaHelper.openAdWall(this);
        else {
            startActivity(new Intent(this, GamezopActivity.class));
        }
    }


}
