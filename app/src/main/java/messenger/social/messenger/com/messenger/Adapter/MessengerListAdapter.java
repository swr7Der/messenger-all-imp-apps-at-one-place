package messenger.social.messenger.com.messenger.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import messenger.social.messenger.com.messenger.Activities.GamezopActivity;
import messenger.social.messenger.com.messenger.Models.BannerHolder;
import messenger.social.messenger.com.messenger.Models.AdHolder;
import messenger.social.messenger.com.messenger.Models.MessengerItemHolder;
import messenger.social.messenger.com.messenger.Models.RecyclerHolder;
import messenger.social.messenger.com.messenger.Models.SingleAd;
import messenger.social.messenger.com.messenger.Models.SingleGame;
import messenger.social.messenger.com.messenger.Models.SinglePackage;
import messenger.social.messenger.com.messenger.R;


public class MessengerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Object> packageName;
    private Context context;
    private LayoutInflater inflater;
    private static final int V1 = 1;
    private static final int V2 = 2;
    private static final int V3 = 3;
    private static final int V4 = 4;
    clickCallback callback;


    public MessengerListAdapter(Context context, ArrayList<Object> packageName) {
        this.packageName = packageName;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setCallback(clickCallback callback) {
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        if (viewType == V1) {

            View view = inflater.inflate(R.layout.single_messenger, viewGroup, false);
            viewHolder = new MessengerItemHolder(view, 0);

        } else if (viewType == V2) {

            View view2 = inflater.inflate(R.layout.single_messenger2, viewGroup, false);
            viewHolder = new AdHolder(view2);
        } else if (viewType == V3) {

            View view3 = inflater.inflate(R.layout.single_messenger5, viewGroup, false);
            viewHolder = new RecyclerHolder(view3);

        } else {

            View view4 = inflater.inflate(R.layout.single_messenger4, viewGroup, false);
            viewHolder = new BannerHolder(view4);
        }

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == 1) {
            configureViewHolder1((MessengerItemHolder) holder, position);
        } else if (holder.getItemViewType() == 2)
            configureViewHolder2((AdHolder) holder);
        else if (holder.getItemViewType() == 3)
            configureViewHolder3((RecyclerHolder) holder);
        else
            configureViewHolder4((BannerHolder) holder);


    }

    private void configureViewHolder3(RecyclerHolder holder) {


    }

    private void configureViewHolder4(BannerHolder holder) {

        holder.seeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, GamezopActivity.class));
            }
        });
    }

    private void configureViewHolder2(AdHolder holder) {

        holder.bindData();
    }

    private void configureViewHolder1(MessengerItemHolder vh1, int position) {

        final Object currPackage = packageName.get(position);

        vh1.bindData(currPackage);

        vh1.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.click(currPackage);
            }
        });
    }


    @Override
    public int getItemViewType(int position) {

        if (packageName.get(position) instanceof Integer)
            return V3;
        else if (packageName.get(position) instanceof String || packageName.get(position) instanceof SinglePackage || packageName.get(position) == null ||
                packageName.get(position) instanceof SingleGame)
            return V1;
         else if (packageName.get(position) instanceof SingleAd)
            return V2;
        else
            return V4;

    }

    @Override
    public int getItemCount() {
        return packageName == null ? 0 : packageName.size();
    }

    public interface clickCallback {

        void click(Object pos);
    }
}
