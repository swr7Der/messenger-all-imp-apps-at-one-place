package messenger.social.messenger.com.messenger.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import messenger.social.messenger.com.messenger.Models.SingleGame;

/**
 * Created by mohak on 24/1/17.
 */

public class IOHelper {

    /**
     * edit shared preference file
     *
     * @param fetch             boolean to write to file
     * @param sharedPreferences sharedPref object for respective file
     */
    public static void sharedPrefEdit(SharedPreferences sharedPreferences, boolean fetch, String key) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, fetch);
        editor.apply();
        Log.d(Constants.TAG, "written " + fetch);
    }

    /**
     * share app link with others
     *
     * @param context activity context
     */
    public static void shareApp(Context context) {

        String message = "Access all your messenger apps at one place. Get the app : https://play.google.com/store/apps/details?id=messenger.social.messenger.com.messenger";
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(share, "Share App Link"));
    }

    public  static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static void writeToFile(List<SingleGame> packageName, Context context, String fileName) {

        FileOutputStream outputStream = null;
        try {
            File file = new File(context.getFilesDir()+"/"+fileName);
            outputStream = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject(packageName);
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.d(Constants.TAG,"write "+ ex);
        }
    }

    public static List<SingleGame> readfromFile(String fileName , Context context) {

        FileInputStream fileInputStream;
        List<SingleGame> packages = null;
        try {
            File file = new File(context.getFilesDir()+"/"+fileName);
            fileInputStream = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fileInputStream);
            packages = (List<SingleGame>) ois.readObject();
            ois.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.d(Constants.TAG,"read "+ ex);
        }
        return packages;
    }






}
