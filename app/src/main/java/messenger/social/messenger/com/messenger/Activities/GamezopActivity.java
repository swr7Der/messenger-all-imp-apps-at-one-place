package messenger.social.messenger.com.messenger.Activities;

/**
 * Created by mohak on 9/2/17.
 */

import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import messenger.social.messenger.com.messenger.Adapter.GamezopAdapter;
import messenger.social.messenger.com.messenger.Helper.Constants;
import messenger.social.messenger.com.messenger.Helper.GameFetcherService;
import messenger.social.messenger.com.messenger.Helper.IOHelper;
import messenger.social.messenger.com.messenger.Helper.Messenger;
import messenger.social.messenger.com.messenger.Models.Game;
import messenger.social.messenger.com.messenger.Models.SingleGame;
import messenger.social.messenger.com.messenger.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GamezopActivity extends AppCompatActivity {

    private RecyclerView gameZop;
    private ProgressBar progressBar;
    private List<SingleGame> gameZopdata;
    private Toolbar mToolBar;
    private GridLayoutManager manager;
    private GamezopAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamezop);

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);

        if (mToolBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        gameZop = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);


        if (IOHelper.isNetworkAvailable(getApplicationContext()) && !new File(getFilesDir() + "/cache").exists()) {
            fetchDatafromGameZop();
        } else if (new File(getFilesDir() + "/cache").exists()){

            progressBar.setVisibility(View.GONE);
            gameZopdata = IOHelper.readfromFile("cache", this);
            setUpManager();
        }else {
            Snackbar.make(gameZop, "No Internet Connection", BaseTransientBottomBar.LENGTH_INDEFINITE).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * set up grid layout manager for list
     */
    private void setUpManager() {

        manager = new GridLayoutManager(this, 3);
        gameZop.setLayoutManager(manager);
        adapter = new GamezopAdapter(gameZopdata, this);
        gameZop.setAdapter(adapter);

    }

    /**
     * Fetches data from gamezop Api
     */
    void fetchDatafromGameZop() {

        progressBar.setVisibility(View.VISIBLE);
        GameFetcherService gameFetcherService = Messenger.getRetroInstance().create(GameFetcherService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id", Constants.API_KEY);
        param.put("appendParams", "true");
        gameFetcherService.getTasks(param).enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {

                if (response.body() != null) {

                    gameZopdata = (Arrays.asList(response.body().getGames()));
                    setUpManager();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {

                Log.d(Constants.TAG, "" + t);
            }
        });

    }


}