package messenger.social.messenger.com.messenger.Activities;

/**
 * Created by mohak on 9/2/17.
 */

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import messenger.social.messenger.com.messenger.Helper.Constants;
import messenger.social.messenger.com.messenger.R;

public class WebViewZop extends AppCompatActivity {

    WebView mWebView;
    FrameLayout mWebContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        mWebContainer = (FrameLayout) findViewById(R.id.web_container);
        mWebView = new WebView(getApplicationContext());
        mWebContainer.addView(mWebView);

        startWebView(getIntent().getStringExtra(Constants.gameLink));

    }

    /**
     * load url in WebView
     *
     * @param url Url of the game
     */
    private void startWebView(final String url) {

        mWebView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            @Override
            public boolean shouldOverrideUrlLoading(android.webkit.WebView view, WebResourceRequest request) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onLoadResource(android.webkit.WebView view, String url) {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(WebViewZop.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            @Override
            public void onPageFinished(android.webkit.WebView view, String url) {

                progressDialog.dismiss();
            }

        });

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.loadUrl(url);
    }


    @Override
    protected void onDestroy() {
        mWebView.destroy();
        deleteDatabase("webviewCache.db");
        super.onDestroy();
    }
}