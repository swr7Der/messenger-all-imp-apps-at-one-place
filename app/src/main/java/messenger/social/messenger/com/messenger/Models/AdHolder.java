package messenger.social.messenger.com.messenger.Models;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import messenger.social.messenger.com.messenger.Helper.MobVistaHelper;
import messenger.social.messenger.com.messenger.R;

/**
 * Created by mohak on 5/2/17.
 */

public class AdHolder extends RecyclerView.ViewHolder {

    ImageView adView;
    TextView ad_app;
    Button button;
    CardView cardView;

    public AdHolder(View itemView) {
        super(itemView);
        adView = (ImageView) itemView.findViewById(R.id.adView);
        ad_app = (TextView) itemView.findViewById(R.id.ad_app_name);
        button = (Button) itemView.findViewById(R.id.button);
        cardView = (CardView) itemView.findViewById(R.id.card);
    }

    public void bindData() {

        MobVistaHelper.loadNative(itemView.getContext(), adView, ad_app , button , cardView, null);
    }


}
