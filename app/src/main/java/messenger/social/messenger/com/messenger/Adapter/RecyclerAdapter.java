package messenger.social.messenger.com.messenger.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import messenger.social.messenger.com.messenger.Activities.GamezopActivity;
import messenger.social.messenger.com.messenger.Models.AdHolder;
import messenger.social.messenger.com.messenger.Models.BannerHolder;
import messenger.social.messenger.com.messenger.Models.Game;
import messenger.social.messenger.com.messenger.Models.MessengerItemHolder;
import messenger.social.messenger.com.messenger.Models.RecyclerHolder;
import messenger.social.messenger.com.messenger.Models.SingleAd;
import messenger.social.messenger.com.messenger.Models.SingleGame;
import messenger.social.messenger.com.messenger.Models.SinglePackage;
import messenger.social.messenger.com.messenger.R;
import retrofit2.Callback;


public class RecyclerAdapter extends RecyclerView.Adapter<MessengerItemHolder> {

    private List<SingleGame> packageName;
    private Context context;
    private LayoutInflater inflater;
    clickCallback callback;


    public RecyclerAdapter(Context context, List<SingleGame> packageName) {
        this.packageName = packageName;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setCallback(clickCallback callback) {
        this.callback = callback;
    }

    @Override
    public MessengerItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = inflater.inflate(R.layout.single_messenger6, viewGroup, false);
        return new MessengerItemHolder(view, 0);
    }

    @Override
    public void onBindViewHolder(MessengerItemHolder holder, int position) {

        configureViewHolder1(holder, position);
    }


    private void configureViewHolder1(MessengerItemHolder vh1, int position) {

        final Object currPackage = packageName.get(position);

        vh1.bindData(currPackage);

        vh1.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callback.click(currPackage);
            }
        });
    }


    @Override
    public int getItemCount() {
        return packageName == null ? 0 : packageName.size();
    }


    public interface clickCallback {

        void click(Object pos);
    }
}
