package messenger.social.messenger.com.messenger.Models;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import messenger.social.messenger.com.messenger.R;

/**
 * Created by mohak on 9/2/17.
 */
public class BannerHolder extends RecyclerView.ViewHolder {

    public TextView seeAll;
    public BannerHolder(View itemView) {

        super(itemView);
        seeAll = (TextView) itemView.findViewById(R.id.seeAll);
    }


}
