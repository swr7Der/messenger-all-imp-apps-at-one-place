package messenger.social.messenger.com.messenger.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import messenger.social.messenger.com.messenger.Helper.Constants;
import messenger.social.messenger.com.messenger.Helper.Database;


/**
 * Created by mohak on 5/2/17.
 */
public class PackageReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {


        String packageName=intent.getData().getEncodedSchemeSpecificPart();

        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {


            for (int i = 0; i < Constants.packageName.length; i++) {

                if (Constants.packageName[i].equals(packageName)) {
                    if (delete(Database.getInstance(context.getApplicationContext()), packageName)) {
                        /*deleted*/
                    }
                    break;
                }
            }
        } else if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {

            for (int i=0;i<Constants.packageName.length;i++){

                if (Constants.packageName[i].equals(packageName)){

                    addTodb(Database.getInstance(context.getApplicationContext()), packageName);
                    break;
                }
            }

        }
    }

    public boolean delete(Database instance, String appPackage) {

        SQLiteDatabase sqldb = instance.getWritableDatabase();
        int x = sqldb.delete(Database.TABLENAME, Database.PACKAGENAME + "=?", new String[]{appPackage});
        return x > 0;
    }


    public static void addTodb(Database instance, String packageName) {

        Cursor cursor = instance.getReadableDatabase().query(Database.TABLENAME, new String[]{Database.PACKAGENAME}, Database.PACKAGENAME + "=?", new String[]{packageName}, null, null, null);
        if (cursor != null && cursor.getCount() == 0) {

            SQLiteDatabase sqldb = instance.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Database.PACKAGENAME, packageName);
            values.put(Database.COUNTER, 0);
            sqldb.insert(Database.TABLENAME, null, values);
        }

        if (cursor != null) {
            cursor.close();
        }

    }


}
