package messenger.social.messenger.com.messenger.Helper;

import android.app.NativeActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobvista.msdk.MobVistaConstans;
import com.mobvista.msdk.MobVistaSDK;
import com.mobvista.msdk.out.Campaign;
import com.mobvista.msdk.out.Frame;
import com.mobvista.msdk.out.MobVistaSDKFactory;
import com.mobvista.msdk.out.MvNativeHandler;
import com.mobvista.msdk.out.MvWallHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import messenger.social.messenger.com.messenger.R;

/**
 * Created by mohak on 1/2/17.
 */

public class MobVistaHelper {

    /**
     * preLoader for mobVista sdk
     */
    public static void preloadWall() {
        MobVistaSDK sdk = MobVistaSDKFactory.getMobVistaSDK();
        Map<String, Object> preloadMap = new HashMap<String, Object>();
        preloadMap.put(MobVistaConstans.PROPERTIES_LAYOUT_TYPE, MobVistaConstans.LAYOUT_APPWALL);
        preloadMap.put(MobVistaConstans.PROPERTIES_UNIT_ID, "5957");
        preloadMap.put(MobVistaConstans.ID_MY_TARGET_AD_UNITID, "5957");
        sdk.preload(preloadMap);
    }

    /**
     * Loader for mobVista sdk
     *
     * @param context Context of calling activity or fragment
     */
    public static void loadHandler(Context context) {
        Map<String, Object> properties = MvWallHandler.getWallProperties("5957");
        properties.put(MobVistaConstans.ID_MY_TARGET_AD_UNITID, "5957");
        MvWallHandler mvHandler = new MvWallHandler(properties, context);
        mvHandler.load();
    }

    /**
     * open ad activity
     *
     * @param context activity context
     */
    public static void openAdWall(Context context) {

        try {
            Class<?> aClass = Class
                    .forName("com.mobvista.msdk.shell.MVActivity");
            Intent intent = new Intent(context, aClass);
            intent.putExtra(MobVistaConstans.PROPERTIES_UNIT_ID, "5957");
            context.startActivity(intent);
        } catch (Exception e) {
            Log.e("MVActivity", "", e);

        }
    }

    public static void preloadNative() {

        MobVistaSDK sdk = MobVistaSDKFactory.getMobVistaSDK();
        Map<String, Object> preloadMap = new HashMap<String, Object>();
        //ads forms parameter（required）
        preloadMap.put(MobVistaConstans.PROPERTIES_LAYOUT_TYPE,
                MobVistaConstans.LAYOUT_NATIVE);
        //Mobvista ad unit id (required)
        preloadMap.put(MobVistaConstans.PROPERTIES_UNIT_ID, "5956");
        //Image preloaded
        preloadMap.put(MobVistaConstans.PREIMAGE, true);
        //Ad number you requested
        preloadMap.put(MobVistaConstans.PROPERTIES_AD_NUM, 4);
//        call preload method
        sdk.preload(preloadMap);
    }

    public static void loadNative(final Context context, final ImageView adView, final TextView appName, @Nullable final Button install, @Nullable final CardView cardView, @Nullable final TextView ad) {

        Map<String, Object> properties = MvNativeHandler
                .getNativeProperties("5956");
        properties.put(MobVistaConstans.PROPERTIES_AD_NUM, 4);
        final MvNativeHandler nativeHandle = new MvNativeHandler(properties, context);
        nativeHandle.setAdListener(new MvNativeHandler.NativeAdListener() {
            @Override
            public void onAdLoaded(List<Campaign> campaigns, int template) {

                if (campaigns != null && campaigns.size() != 0 && campaigns.size() > 0) {

                    new ImageLoadTask(campaigns.get(0).getIconUrl()) {

                        @Override
                        public void onRecived(Drawable result) {
                            // TODO Auto-generated method stub
                            adView.setImageDrawable(result);
                            if (result != null && ad != null)
                                ad.setText("AD");
                            if (result != null && install != null)
                                install.setVisibility(View.VISIBLE);
                        }
                    }.execute();

                    if (cardView != null)
                        cardView.setVisibility(View.VISIBLE);


                    List<View> list = new ArrayList<View>();

                    appName.setText(campaigns.get(0).getAppName());
                    list.add(adView);
                    list.add(appName);
                    if (install != null)
                        list.add(install);

                    nativeHandle.registerView(adView, list, campaigns.get(0));

                    preloadNative();
                }
            }


            @Override
            public void onAdLoadError(String message) {

                if (install != null)
                    install.setVisibility(View.INVISIBLE);

                if (cardView != null)
                    cardView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAdClick(Campaign campaign) {

                Toast.makeText(context, "Redirecting to Play Store", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFramesLoaded(final List<Frame> list) {
            }
        });

        nativeHandle.load();

    }

}
