package messenger.social.messenger.com.messenger.Models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by mohak on 28/1/17.
 */

public class SingleGame implements Serializable {

    @SerializedName("name")
    private String gameName;

    @SerializedName("url")
    private String gameUrl;

    @SerializedName("thumb")
    private String gameImg;

    public String getGameImg() {
        return gameImg;
    }

    public void setGameImg(String gameImg) {
        this.gameImg = gameImg;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameUrl() {
        return gameUrl;
    }

    public void setGameUrl(String gameUrl) {
        this.gameUrl = gameUrl;
    }
}
