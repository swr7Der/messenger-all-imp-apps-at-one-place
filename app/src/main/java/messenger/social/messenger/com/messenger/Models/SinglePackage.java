package messenger.social.messenger.com.messenger.Models;


import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by mohak on 28/1/17.
 */

/**
 * Represents a single Package model.
 * Implements serializable so that object of this class can be written to a file
 */
public class SinglePackage implements Serializable {


    private String packageName;

    private int click_counter;

    private Drawable imgId;

    private static int total_click_counter;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Drawable getImgId() {
        return imgId;
    }

    public void setImgId(Drawable imgId) {
        this.imgId = imgId;
    }

    public int getClick_counter() {
        return click_counter;
    }

    public void setClick_counter(int click_counter) {
        this.click_counter = click_counter;
    }

    public static int getTotal_click_counter() {
        return total_click_counter;
    }

    public static void setTotal_click_counter(int total_click_counter) {
        SinglePackage.total_click_counter = total_click_counter;
    }
}
