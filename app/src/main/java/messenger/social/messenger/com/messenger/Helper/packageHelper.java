package messenger.social.messenger.com.messenger.Helper;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by mohak on 23/1/17.
 */

public class packageHelper {

    /**
     *
     * @param packageName package name of the app
     * @param context use application context to prevent memory leak
     * @return name of the app
     */
    public static String getAppName(String packageName, Context context) {

        String name = null;
        try {
            ApplicationInfo app = context.getApplicationContext().getPackageManager().getApplicationInfo(packageName, 0);
            name = context.getApplicationContext().getPackageManager().getApplicationLabel(app).toString();

        } catch (PackageManager.NameNotFoundException e) {

            //App not installed
        }

        return name;
    }

    /**
     *
     * @param packageName package name of the app
     * @param context use application context to prevent memory leak
     * @return drawable icon of the app
     */
    public static Drawable getAppDrawable(String packageName, Context context) {

        Drawable icon = null;
        try {
            ApplicationInfo app = context.getApplicationContext().getPackageManager().getApplicationInfo(packageName, 0);
            icon = context.getApplicationContext().getPackageManager().getApplicationIcon(app);

        } catch (PackageManager.NameNotFoundException e) {

            //App not installed
        }

        return icon;
    }

    /**
     *
     * @param packagename package name of the app to check for
     * @param packageManager packageManager object
     * @return true if app is installed else false
     */
    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


}
